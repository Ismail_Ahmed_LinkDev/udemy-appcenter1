//
//  MinutesTests.swift
//  MinutesTests
//
//  Created by Ismail Ahmed on 3/25/20.
//  Copyright © 2020 Microsoft. All rights reserved.
//

import XCTest
@testable import Minutes

class MinutesTests: XCTestCase {

    var entryUnderTest: Entry!
    override func setUp() {
        entryUnderTest = Entry("Title", "Content")
    }

    override func tearDown() {
        entryUnderTest = nil
    }
    
    func testSerialization() {
        let data = FileEntryStore.serialize(entryUnderTest)
        let entry = FileEntryStore.deserialize(data!)
        XCTAssertEqual(entry?.title, entryUnderTest.title, "Title doesn't match")
        XCTAssertEqual(entry?.content, entryUnderTest.content, "Content doesn't match")
    }
}
